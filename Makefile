.PHONY: build_and_run build

SOURCES=matrix.cpp test.cpp
OBJECTS=$(SOURCES:.cpp=.o)
BUILD_FOLDER=Build/

all: build

build_and_run: build
	./$(BUILD_FOLDER)run_test

build: prepare $(OBJECTS)
	g++ $(BUILD_FOLDER)*.o -o $(BUILD_FOLDER)run_test

prepare:
	mkdir -p $(BUILD_FOLDER)

.cpp.o:
	g++ -c $< -o $(BUILD_FOLDER)$@