#include"matrix.hpp"
#include<iostream>
#include<vector>


int main(int argc, char* argv[])
{
    // Matrix a(3, 3, );
    std::vector<std::vector<double>> check_vector{{{1, 2, 3}, 
                                                {2, 3, 4}, 
                                                {3, 4, 5}}};
    // Matrix a(check_vector); 
    Matrix a;
    std::cin >> a;                                            
    Matrix b(std::vector<std::vector<double>> {{{7, 8, 2}, 
                                                {0, 1, 4}, 
                                                {9, 3, 1}}});
    std::cout << a * b;
    return 0;
}