#include"matrix.hpp"


Matrix::Matrix()
{
    this->m = 0;
    this->n = 0;
    this->a = nullptr;
}


Matrix::Matrix(const int& m, const int& n, const double ** data)
{
    this->m = m;
    this->n = n;
    this->a = new double*[m];
    for (int i = 0; i < m; ++i)
    {
        this->a[i] = new double[n];
        for (int j = 0; j < n; ++j)
            if (data != nullptr)
                (*this)[i][j] = data[i][j];
            else
                (*this)[i][j] = 0;
    }
}


Matrix::Matrix(const std::vector<std::vector<double>>& matrix_data)
{
    if (matrix_data.size() == 0)
        throw "Wrong input data";
    if (matrix_data.at(0).size() == 0)
        throw "Wrong input data";

    this->m = matrix_data.size();
    this->n = matrix_data.at(0).size();
    this->a = new double*[this->get_hight()];
    for (int i = 0; i < this->get_hight(); ++i)
    {
        this->a[i] = new double[this->get_width()];
        for (int j = 0; j < this->get_width(); ++j)
        {
            (*this)[i][j] = 0;
            (*this)[i][j] = matrix_data.at(i).at(j);
        }
    }
}


Matrix::Matrix(const Matrix& copy_matr)
{
    this->m = copy_matr.m;
    this->n = copy_matr.n;
    this->a = new double*[m];
    for (int i = 0; i < m; ++i)
    {
        this->a[i] = new double[n];
        for (int j = 0; j < n; ++j)
            (*this)[i][j] = copy_matr.a[i][j];
    }
}


Matrix::~Matrix()
{
    for (int i = 0; i < m; ++i)
        delete[]this->a[i];
    delete[]this->a;
    this->a = nullptr;
    this->m = 0;
    this->n = 0;
}


Matrix& Matrix::multiply(const int& multiplier) const
{
    Matrix* result = new Matrix(this->m, this->n);
    for (int i = 0; i < this->m; ++i)
        for (int j = 0; j < this->n; ++j)
            (*result)[i][j] *= multiplier;
    
    return *result;
}


Matrix& Matrix::multiply(const double& multiplier) const
{
    Matrix* result = new Matrix(this->m, this->n);
    for (int i = 0; i < this->m; ++i)
        for (int j = 0; j < this->n; ++j)
            (*result)[i][j] *= multiplier;
    
    return *result;
}


Matrix& Matrix::operator*(const int& multiplier) const
{
    return multiply(multiplier);
}


Matrix& Matrix::operator*(const double& multiplier) const
{
    return multiply(multiplier);
}


Matrix& Matrix::multiply(const Matrix& multiplier) const
{
    if (this->n != multiplier.m)
        throw std::invalid_argument("Matrixes are not compatible.");
    
    Matrix* result = new Matrix(this->m, multiplier.n);

    for (int i = 0; i < result->m; ++i)
        for (int j = 0; j < result->n; ++j)
            for (int k = 0; k < this->n; ++k)
            {
                (*result)[i][j] += (*this)[i][k] * multiplier[k][j];
            }
    
    return *result;
}


Matrix& Matrix::operator*(const Matrix& multiplier) const
{
    return multiply(multiplier);
}


Matrix& Matrix::addition(const Matrix& matrix) const
{
    if (this->get_hight() != matrix.get_hight() || this->get_width() != matrix.get_width())
        throw "Matrixes are not compatible.";

    Matrix* result = new Matrix(this->get_hight(), this->get_width());

    for (int i = 0; i < this->get_hight(); ++i)
        for (int j = 0; j < this->get_width(); ++j)
            (*result)[i][j] = (*this)[i][j] + matrix[i][j];

    return *result;
}


Matrix& Matrix::operator+(const Matrix& matrix) const
{
    return addition(matrix);
}


Matrix& Matrix::T() const
{
    Matrix* result = new Matrix(this->get_width(), this->get_hight());

    for (int i = 0; i < this->get_hight(); ++i)
        for (int j = 0; j < this->get_width(); ++j)
            (*result)[j][i] = (*this)[i][j];
    
    return *result;
}


double& Matrix::trace() const
{
    if (this->get_hight() != this->get_width())
        throw "Square matrix required";
    double *result = new double(0);

    for (int i = 0; i < this->get_hight(); ++i)
        *result += (*this)[i][i];
    
    return *result;
}


double& Matrix::det() const
{
    // TODO
    return *(new double(0));
} 


Matrix& Matrix::operator=(const Matrix& matrix)
{
    for (int i = 0; i < this->get_hight(); ++i)
        delete [] this->a[i];
    delete [] *this->a;

    this->m = matrix.get_hight();
    this->n = matrix.get_width();
    for (int i = 0; i < this->get_hight(); ++i)
        for (int j = 0; j < this->get_width(); ++j)
            (*this)[i][j] = matrix[i][j];
    
    return *this;
}


double Matrix::get_element_value(const int& i, const int& j) const
{
    if (i < 0 || i >= this->get_hight() || j < 0 || j >= this->get_width())
        throw "Wrong index";

    return (*this)[i][j];
}


int Matrix::get_hight() const
{
    return this->m;
}


int Matrix::get_width() const
{
    return this->n;
}


void Matrix::set_element_value(const int& i, const int& j, const double& value)
{
    if (i < 0 || i >= this->get_hight() || j < 0 || j >= this->get_width())
        throw "Wrong index";
    
    (*this)[i][j] = value;
}


Matrix& Matrix::resize(const int& new_m, const int& new_n) const
{
    if (new_m <= 0 || new_n <= 0)
        throw "Wrong size";

    Matrix* result = new Matrix(new_m, new_n);
    int counter_m = 0, counter_n = 0;
    bool new_shape_end = false;
    for (int i = 0; i < this->get_hight() && !new_shape_end; ++i)
        for (int j = 0; j < this->get_width() && !new_shape_end;++j)
        {
            (*result)[counter_m][counter_n] = (*this)[i][j];
            if (++counter_m == new_m)
            {
                counter_m = 0;
                if (++counter_n == new_n)
                    new_shape_end = true;
            }
        }
    
    return *result;
}

double* Matrix::operator[](const int& index) const
{
    if (index < 0 || index >= this->get_hight())
        throw "Wrong index";
    
    return this->a[index];
}

// fritend functions
Matrix& operator*(const int& multiplier, const Matrix& matrix)
{
    return matrix*multiplier;
}


Matrix& operator*(const double& multiplier, const Matrix& matrix)
{
    return matrix*multiplier;
}