#ifndef MATRIX_HPP
#define MATRIX_HPP
#include<vector>
#include<iostream>


// TODO: logging instead of cout

class Matrix
{
    private:
        int m, n; // m - number of lines, n - number of columns
        double **a; // a - matrix
    public:
        Matrix();
        Matrix(const int& m, const int& n, const double ** data = nullptr);
        Matrix(const std::vector<std::vector<double>>& matrix_data);
        Matrix(const Matrix& copy_matr);
        ~Matrix();

        Matrix& multiply(const int& multiplier) const;
        Matrix& multiply(const double& multiplier) const;
        Matrix& operator*(const int& multiplier) const;
        Matrix& operator*(const double& multiplier) const;
        friend Matrix& operator*(const int& multiplier, const Matrix& matrix);
        friend Matrix& operator*(const double& multiplier, const Matrix& matrix);
        
        Matrix& multiply(const Matrix& multiplier) const;
        Matrix& operator*(const Matrix& multiplier) const;

        Matrix& addition(const Matrix& matrix) const;
        Matrix& operator+(const Matrix& matrix) const;

        Matrix& T() const; // transpose

        double& trace() const;

        double& det() const; // determinant

        Matrix& operator=(const Matrix& matrix);

        double get_element_value(const int& i, const int& j) const;
        int get_hight() const;
        int get_width() const;

        void set_element_value(const int& i, const int& j, const double& value);

        Matrix& resize(const int& new_m, const int& new_n) const;

        double* operator[](const int& index) const;

        friend std::istream& operator>>(std::istream& input, Matrix& matrix)
        {
            input >> matrix.m >> matrix.n;
            matrix.a = new double*[matrix.m];
            for (int i = 0; i < matrix.m; ++i){
                matrix.a[i] = new double[matrix.n];
                for (int j = 0; j < matrix.n; ++j){
                    input >> matrix.a[i][j];
                }
            }
            return input;
        }

        friend std::ostream &operator<<( std::ostream &output, const Matrix& matrix) 
        { 
            for (int i = 0; i < matrix.get_hight(); ++i)
            {
                for (int j = 0; j < matrix.get_width(); ++j)
                    output << matrix[i][j] << " ";
                output << std::endl;
            }
            return output;            
        }
};

Matrix& operator*(const int& multiplier, const Matrix& matrix);
Matrix& operator*(const double& multiplier, const Matrix& matrix);

#endif // MATRIX_HPP